# SAMD21 demo

This is mostly a proof-of-concept of a USB Host driver on the SAMD21
platform.

The code is also a complete mess, but is hopefully understandable
enough. At some point I intend to separate this into architecture
dependent and independent stuff with a HAL layer. However, right now,
this is mostly just slapped together as I tried to figure out how to
get stuff to work at all.

# Features

Basically none. It'll go through enumeration and, at least on my
Logitech G105 keyboard, will get boot protocol keyboard reports and
print them out to the UART.

# Board support

Uses trinket-m0, but should be fairly easy to port to other samd
architectures.

# Building

Normal cargo stuff works. There's a `Makefile` which has a target for
flashing an Adafruit Trinket M0 on Linux.

You'll need to tweak the atsamd-hal to remove the feature requirements
for generating the USB padcal code. You'll also need to add
definitions for the 1kHz SOF pad, and (optionally) the USB Host Enable
PIN. You can find a version of the HAL that does this [on my github
branch](https://github.com/bjc/atsamd/tree/unflag-usb-calibration).
